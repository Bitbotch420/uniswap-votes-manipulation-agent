const {
    GOV_ADDRESS,
    UNISWAP_ADDRESS,
    VOTE_SIGNATURE
} = require("./vars");

const { Finding, FindingSeverity, FindingType, getEthersProvider, ethers, BigNumber} = require("forta-agent");

async function handleNewVotes(txEvent, contracts, voters) {
      //deconstruct data globals
      const {UNISWAP_CONTRACT, GOV_CONTRACT} = contracts;
      //Findings array
      const findings = [];
  
      if(!UNISWAP_CONTRACT || !GOV_CONTRACT) throw new Error("handleNewVotes called before contracts init");
      //If the txEvent doesn't relate to the governance address for the votes then discard it
      if (!txEvent.addresses[GOV_ADDRESS.toLowerCase()]) return findings;
      //check for VoteCast events
      const voteLogs = txEvent.filterLog(VOTE_SIGNATURE, GOV_ADDRESS);
      //Logs include VoteCast
      if(voteLogs.length > 0) {
        //deconstruct & parse the log easier for data access
        const {args: {voter, proposalId, votes}} = voteLogs[0];
        //get starting snapshot block proposal
        const {startBlock} = await GOV_CONTRACT.proposals(proposalId.toNumber());
        console.log(`Proposal started at ${startBlock} and user casted ${ethers.utils.formatUnits(votes, 18)} UNI`);
        //create array of block numbers from 100 before the starting block
        const hundredBlocksBefore = [...Array(100).keys()].map(i => startBlock - (i+1));
        //fetch vote count for each block
        let votesAcrossBlocks = await Promise.all(hundredBlocksBefore.map(i => UNISWAP_CONTRACT.getPriorVotes(voter, i)));
        //filter the votes to see if has any signiicant change
        for(let [idx, value] of votesAcrossBlocks.entries()) {
          if(idx !== 0) {
            const voteDifference = value.sub(votesAcrossBlocks[idx-1]);
            if(voteDifference.gt(0)) {
              const changePercentage = voteDifference.div(votesAcrossBlocks[idx-1]).mul(100);
              if(changePercentage.gt(25)) {
                //Push into the findings array 
                findings.push(Finding.fromObject({
                  name: "UNI Governance",
                  description: `${voter} votes increased by ${changePercentage} 100 blocks leading up to the snapshot starting block`,
                  alertId: "UNISWAP-1",
                  type: FindingType.Suspicious,
                  severity: FindingSeverity.High,
                  metadata: {
                   voteValue: value, 
                   changeInVotes: changePercentage,
                   atBlock: hundredBlocksBefore[idx],
                  },
                }));
                break;
              }        
            }
          }
        }
        //monitor vote balance 100 blocks after its casted
        voters.push({voter, votes, blockNumber: txEvent.receipt.blockNumber});
      }
      return findings;
}


module.exports.handleNewVotes = handleNewVotes;