const { Finding, FindingSeverity, FindingType, getEthersProvider, ethers, BigNumber} = require("forta-agent");
async function checkOldVotes(blockEvent, contracts, voters) {
    const {UNISWAP_CONTRACT} = contracts;
    const blockNumber = blockEvent.block.number;
    const withinHundredBlocks = voters.filter(obj => ((obj.blockNumber + 100) >= blockNumber) && !obj.triggered);
    const findings = [];
    for(let v of withinHundredBlocks) {
        const getVotes = await UNISWAP_CONTRACT.getCurrentVotes(v.address);
        const changeOfVotes = v.votes.sub(getVotes);
        if(changeOfVotes.gt(0)) {
            findings.push(Finding.fromObject({
                name: "Uni Governance",
                description: `Decrease in votes after VoteCast from ${v.address}`,
                alertId: "UNISWAP-2",
                type: FindingType.Suspicious,
                severity: FindingSeverity.High,
                metadata: {
                 changeInVotes: changeOfVotes,
                 atBlock: blockNumber
                },
              }));
            this.voters[this.voters.findIndex(i => i.address == v.address && i.blockNumber == v.blockNumber)].triggered = true;
        }
    }
    return findings;
}
module.exports.checkOldVotes = checkOldVotes;