const UNISWAP_ABI = require("../abi/uniswap.json");
const GOV_ABI = require("../abi/gov.json");
const [GOV_ADDRESS, UNISWAP_ADDRESS] = ["0x408ED6354d4973f66138C91495F2f2FCbd8724C3", "0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984"]
const VOTE_SIGNATURE = "event VoteCast(address indexed voter, uint proposalId, uint8 support, uint votes, string reason)";

module.exports = {UNISWAP_ABI, GOV_ABI, GOV_ADDRESS, UNISWAP_ADDRESS, VOTE_SIGNATURE};
